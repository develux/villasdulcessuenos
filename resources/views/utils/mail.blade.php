<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reservacion</title>
</head>
<body>
<div align="center">
    <img src="{{asset('/img/logo.png')}}" alt="">
</div>
<p>Estimado(a) Sr(a). {{$cliente->nombre}}</p>
<p>Gracias por su eleccion en Villas Dulce Sueños, esperamos cumplir con sus espectativas.</p>
<br>
<p>El folio de su reservación es:</p>
<div align="center">
    <h2>{{$datos->folio}}</h2>
</div>
<br>
<p>Reservando la habitación #{{$habitacion->numero}}</p>
<p>para el periodo de {{substr($datos->fechallegada,0,10).' a '.substr($datos->fechasalida,0,10)}}</p>
<br>
<p>Dejamos a su disposicion nuestras <a href="{{asset('docs/politicas.pdf')}}">politicas de privacidad</a></p>
<iframe src="{{asset('docs/politicas.pdf')}}" width="600" height="780" style="border: none;"></iframe>

</body>
</html>
