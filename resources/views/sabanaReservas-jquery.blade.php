@extends('layout.admin')
@section('styles')
    <style>
        @media print {
            @page {
                size: landscape
            }
        }

        table, td, th {
            border: 1px solid black;

        }

    </style>
@endsection
@section('content')
    @include('partials.navbar', ['ventana' => 'Sabana de Reservas','name'=>'SabanaReservas'])

    @php
        switch ($mes) {
            case '01':
                $mesname = "Enero";
                break;
            case '02':
                $mesname = "Febrero";
                break;
            case '03':
                $mesname = "Marzo";
                break;
            case '04':
                $mesname = "Abril";
                break;
            case '05':
                $mesname = "Mayo";
                break;
            case '06':
                $mesname = "Junio";
                break;
            case '07':
                $mesname = "Julio";
                break;
            case '08':
                $mesname = "Agosto";
                break;
            case '09':
                $mesname = "Septiembre";
                break;
            case '10':
                $mesname = "Octubre";
                break;
            case '11':
                $mesname = "Noviembre";
                break;
            case '12':
                $mesname = "Diciembre";
                break;
        }

    @endphp
    <br>
    <div class="row d-flex justify-content-around">
        <form action="{{action('WebController@previusMonth')}}" method="GET">
            <button type="submit" class="btn btn-primary">&larr;Anterior</button>
        </form>
        <h3 align="center"><a href="#">{{$mesname}}</a></h3>
        <form action="{{action('WebController@nextMonth')}}" method="GET">
            <button class="btn btn-primary">Siguiente&rarr;</button>
        </form>
    </div>
    <br>

    <style>
        .wrap-dias-mes {
            width: 100%;
            height: auto;
            overflow: auto;

            display: table;
        }

        .habitacion-mes {
            width: 100px;
            float: left;
            text-align: center;

        }

        .habitacion-mes small {
            display: grid;
        }

        .dias-mes {
            width: 140px;
            height: 80px;
            overflow: auto;
            float: left;
            text-align: center;

        }

        .dias-mes small {
            display: grid;
        }
    </style>

    <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
    <script>

        $(document).ready(() => {
            let sabana = $("#sabana").val();
            let parseSabana = JSON.parse(sabana);
            console.log(parseSabana);

            /*parseSabana.map((item) => {
                $.each(item.reservaciones, function (i, data) {
                    var current = data.dia;

                    console.log("IDENT " + i);
                    console.log(current);
                });
            });*/

            parseSabana.map((item) => {
                $("#renderSabana").append(renderHabitaciones(item));
            });
        });

        Habitaciones = null;

        function renderHabitaciones(habitacion) {
            contenido = '';
            contenido += `<div style="width: calc(${$("#diasMes").val()} * 140px + 100px); background-color: #605ca8; color: white;" class="habitacion-mes">
                <div class="habitacion-mes">
                    ${habitacion.datos.numero}
                </div>`;

            //Habitaciones = habitacion.reservaciones;
                habitacion.reservaciones.forEach((reserva, i) => {
                    contenido += `<div id="lista-reservaciones">`;
                    current = habitacion.reservaciones[i];
                    if((i + 1) < habitacion.reservaciones.length) {
                        next = habitacion.reservaciones[i + 1];
                        prev = habitacion.reservaciones[i - 1];

                        /*if(current.folio) {
                            console.log("YO si tengo folio");
                        }else{
                            console.log("YO no tengo folio");
                        }*/
                        medida = "140px";
                        color = 'transparent';
                        if(current.folio) {
                            color = 'rgb(61, 59, 105)';
                            if(current.dias > 0) {
                                habitacion.reservaciones.splice(i, current.dias - 1);
                                medida = `calc(140px * ${current.dias})`;
                                console.log("mi objeto");
                            } else {
                                medida = `140px`;
                            }
                        } else {
                            color = 'transparent';
                        }


                        if(next.folio && prev.folio) {
                            medida = "140px";
                        }else if(next.folio && !current.folio && prev.folio) {
                            medida = "140px";
                        }else if(next.folio && !current.folio) {
                            if(next.dias > 0) {
                                medida = "calc(140px + 70px)";
                            } else {
                                medida = "140px";
                            }


                        }

                        //if(next.folio) {
                            /*if(next.dias > 0) {
                                //medida = "calc(140px + 70px)";
                                medida = "140px";
                            } else {
                                medida = "140px";
                            }*/

                            /*if(prev.folio) {
                                alert("Hay atras" + current.dia);
                            }

                            /*if(prev.folio) {
                                medida = "140px";
                            } else {
                                if(next.dias > 0) {
                                    medida = "calc(140px + 10px)";
                                } else {
                                    medida = "140px";
                                }
                            }*/
                            //console.log("El siguiente tiene folio" + next.folio);
                            /*console.log(habitacion.reservaciones[i - 1].dia);
                        } else if(next.folio && prev.folio) {

                            console.log("POS SI HAY");
                            medida = "140px";
                        }*/



                        /*if(next.folio && prev.folio) {
                            medida = "140px";
                        }*/





                        if(current.folio) {
                            contenido += `<div style="width: ${medida}; background-color: ${color};" data-tipo="reservacion" data-dias="${reserva.dias}" class="dias-mes">
                            <small>
                            ${reserva.folio} <br />
                            ${reserva.fechallegada} <br />
                            ${reserva.fechasalida}
                            </small>
                            </div>`;
                        } else {
                            contenido += `<div style="width: ${medida}; background-color: ${color};" data-tipo="libre" class="dias-mes">
                            <small>
                            ${reserva.dia}
                            </small>
                            </div>`;
                        }


                        //console.log("Mi medida: " + medida);
                    }


                    //console.log(habitacion.reservaciones[i]);
                    /*$.each(reserva, function (i, data) {
                        console.log(data[1]);
                    });*/
                        /*var current = data.dia;

                        console.log("IDENT " + i);
                        console.log(current);

                        /*if(data[i + 1].folio) {

                            console.log(data.dia + "Tiene folio");
                        } else {
                            console.log(data.dia + "No tiene folio");
                        }
                    });*/
                    /*contenido += `<div id="lista-reservaciones">`;
                    if (reserva.folio) {
                        contenido += `<div data-tipo="reservacion" data-dias="${reserva.dias}" class="dias-mes">
                        <small>
                        ${reserva.folio}
                        ${reserva.fechallegada}
                        ${reserva.fechasalida}
                        </small>
                        </div>`;
                    } else {
                                contenido += `<div data-tipo="libre" class="dias-mes">
                        <small>
                        ${reserva.dia}
                        </small>
                        </div>`;
                    }*/
                    contenido += `</div>`;
                });
                `</div>
                </div>`;
            return contenido;
        }

        function renderReservas(reserva) {
            let contenido = '';
            contenido += `<div id="lista-reservaciones">`;
            if (reserva.folio) {
                contenido += `<div data-tipo="reservacion" data-dias="${reserva.dias}" class="dias-mes">
                <small>
                ${reserva.folio}
                ${reserva.fechallegada}
                ${reserva.fechasalida}
                </small>
                </div>`;
            } else {
                contenido += `<div data-tipo="libre" class="dias-mes">
                <small>
                ${reserva.dia}
                </small>
                </div>`;
            }
            contenido += `</div>`;
            return contenido;
        }
    </script>

    <div class="container" style="overflow-x: auto;">
        <div
            style="overflow-x: auto; width: calc({{$diasdelmes}} * 140px + 100px); background-color: #555199; color: white;">
            <div class="habitacion-mes">Habitación</div>
            <?php $dias_ES = array("Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"); ?>
            <?php $dias_EN = array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"); ?>

            @for($dia=1; $dia<=$diasdelmes; $dia++)
                <div class="dias-mes">{{$dia}}
                    <small>{{$dia_ES = str_replace($dias_EN, $dias_ES, date('D', strtotime($year.'-'.$mes.'-'.$dia)))}}</small>
                </div>
            @endfor
        </div>

        <input type="hidden" value="{{$diasdelmes}}" id="diasMes"/>
        <input type="hidden" value="{{$sabana}}" id="sabana"/>

        <div id="renderSabana"></div>
        {{--@foreach($sabana as $sab)
            <div style="width: calc({{$diasdelmes}} * 140px + 100px); background-color: #605ca8; color: white;"
                 class="habitacion-mes">
                <div class="habitacion-mes">
                    {{$sab["datos"]["numero"]}}
                </div>

                <div id="lista-reservaciones">
                    @if(isset($sab["reservaciones"]) && count($sab["reservaciones"]) > 0)
                        @foreach ($sab["reservaciones"] as $reservacion)
                            @if (array_key_exists("folio", $reservacion))
                                <div data-tipo="reservacion" data-dias="{{$reservacion["dias"]}}" class="dias-mes">
                                    <small>
                                        {{$reservacion["folio"]}} <br />
                                        {{$reservacion["fechallegada"]}} <br />
                                        {{$reservacion["fechasalida"]}} <br />
                                    </small>
                                </div>
                            @else
                                <div data-tipo="libre" class="dias-mes">
                                    <small>{{$reservacion["dia"]}}</small>
                                </div>
                            @endif
                        @endforeach
                </div>
                @endif
            </div>
        @endforeach--}}
    </div>
@stop

@section("script")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@1.5.3/dist/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.2.4/dist/jspdf.plugin.autotable.js"></script>
    <script type="text/javascript" src="{{asset('/js/jquery.thead-1.1.js')}}"></script>
    <script>
        $(function () {
            $('table').thead();
        });

        function imprimir() {
            html2canvas($("#contenido"), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL("image/png");
                    console.log(img);

                }

            });
        }
    </script>
@stop
