# INSTRUCCIONES

### 1.- Ejecutar consola de mysql en modo ROOT.
### 2.- Crear base de datos:
```sh
create database u433255739_hotel;
```
### 3.- Crear usuario bd: 
```sh
create user 'u433255739_hotel'@'localhost' identified by 'villadulce';
```
### 4.- Asignar privilegios del usuario a la bd:
```sh
grant all privileges on u433255739_hotel.* to 'u433255739_hotel'@'localhost';
```
### 5.- Importar el respaldo de base de datos:
```sh
mysql -u u433255739_hotel -p u433255739_hotel < u433255739_hotel.20191025071926.sql
```
### 6.- Ingresar al directorio principal y ejecutar
```sh
composer install
```
### 7.- Echar a andar el servidor
```sh
php artisan serve
```
### 8.- Ingresar en el navegador a
(http://localhost:8000/login)

**Usuario**
```sh
adminVDS
```
**Contraseña**
```sh
1234
```
