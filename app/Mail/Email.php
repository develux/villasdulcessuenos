<?php

namespace App\Mail;

use App\Cliente;
use App\Habitacion;
use App\Reserva;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Email extends Mailable
{
    use Queueable, SerializesModels;
    var $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($idR)
    {
        $this->id = $idR;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $reservacion = Reserva::where('folio','=',$this->id)->first();

        $cliente = Cliente::findOrFail($reservacion->idCliente);
        $habitacion = Habitacion::findOrFail($reservacion->idHabitacion);

        return $this->view('utils.mail',["datos"=>$reservacion,'cliente'=>$cliente,'habitacion'=>$habitacion]);
    }
}
